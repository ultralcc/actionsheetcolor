//
//  ViewController.swift
//  ActionSheetColor
//
//  Created by 訪客使用者 on 2017/11/14.
//  Copyright © 2017年 LCC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tap(_ sender: Any) {
        
        let actionSheet = UIAlertController(title:"Chose", message:nil,preferredStyle:UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title:"Blue", style:UIAlertActionStyle.default, handler: { (alertAction)in
            self.view.backgroundColor = UIColor.blue
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Red", style:UIAlertActionStyle.default, handler: { (alertAction)in
            self.view.backgroundColor = UIColor.red
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Green", style:UIAlertActionStyle.default, handler: { (alertAction)in
            self.view.backgroundColor = UIColor.green
        }))
        
        actionSheet.addAction(UIAlertAction(title:"White", style:UIAlertActionStyle.default, handler: { (alertAction)in
            self.view.backgroundColor = UIColor.white
        }))
    
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
}

